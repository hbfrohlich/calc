cmake_minimum_required(VERSION 3.9)
project(calc CXX)

add_executable(calc
    main.cpp
)